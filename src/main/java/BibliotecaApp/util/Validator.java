package BibliotecaApp.util;

import BibliotecaApp.model.Carte;

import java.util.Calendar;

public class Validator {

    private static int currentYear;
    // functie booleana care verifica daca stringul trimis ca parametru contine numai litere din alfabetul englez
	public static boolean isStringOK(String s) throws Exception{
		boolean flag = s.matches("[a-zA-Z ]+");
		if(!flag)
			throw new Exception("String invalid");

		if(s.length() > 256) throw new Exception("String invalid");
		return flag;
	}

	// valideaza structura interna a cartii c trimisa ca parametru
	public static void validateCarte(Carte c)throws Exception{
		if(c.getCuvinteCheie()==null){ //if(c.getCuvinteCheie()!=null){
			throw new Exception("Lista cuvinte cheie vida!");
		}
		if(c.getAutori()==null){
			throw new Exception("Lista autori vida!");
		}
		if(!isStringOK(c.getTitlu()))
			throw new Exception("Titlu invalid!");

		for(String s:c.getAutori()){
			if(!isStringOK(s))
				throw new Exception("Autor invalid!");
		}
		for(String s:c.getCuvinteCheie()){
			if(!isStringOK(s))
				throw new Exception("Cuvant cheie invalid!");
		}
		if(!isNumber(c.getAnAparitie()))
			throw new Exception("An invalid!");
	}

	// functie booleana care verifica daca stringul trimis ca parametru contine numai cifre
	public static boolean isNumber(String s){
		if (s.matches("[0-9]+")){
		 return okYear(s);
        } else return false;
	}

	public static boolean okYear(String s) {
		int an = Integer.parseInt(s);

		currentYear = Calendar.getInstance().get(Calendar.YEAR);
		return an >= 1000 && an < currentYear + 1;
	}
//
//	public static boolean isOKString(String s){
//		String []t = s.split(" ");
//		if(t.length==2){
//			boolean ok1 = t[0].matches("[a-zA-Z]+");
//			boolean ok2 = t[1].matches("[a-zA-Z]+");
//			if(ok1==ok2 && ok1==true){
//				return true;
//			}
//			return false;
//		}
//		return s.matches("[a-zA-Z]+");
//	}
	
}
