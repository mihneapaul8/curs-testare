package BibliotecaApp.util;

import BibliotecaApp.helpers.Colors;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import static org.junit.Assert.assertTrue;

public class ValidatorTest {
    private static String watchedLog = "";

    @Rule
    public TestWatcher watchman = new TestWatcher()
    {
        @Override
        protected void failed(Throwable e, Description description)
        {
            watchedLog+= Colors.ANSI_RED + description + " " + "HAS FAILED " + Colors.ANSI_RESET + "\n";
        }

        @Override
        protected void succeeded(Description description)
        {
            watchedLog+= Colors.ANSI_GREEN + description + " " + "HAS SUCCEEDED " + Colors.ANSI_RESET + "\n";
        }
    };

    @BeforeClass
    public static void setUpClass()
    {
        System.out.println(Colors.ANSI_PURPLE + "\"****** INIT UNIT TESTS ****** \n" + Colors.ANSI_RESET);
    }

    @Test
    public void isStringOKTest() throws Exception
    {
        assertTrue(Validator.isStringOK("test"));
    }

    @Test(expected = java.lang.Exception.class)
    public void stringIsNotOk() throws Exception
    {
        assertTrue(Validator.isStringOK("1234"));
    }

    @AfterClass
    public static void tearDownClass()
    {
        System.out.println(watchedLog);
        System.out.println("Finish \n\n");
    }
}