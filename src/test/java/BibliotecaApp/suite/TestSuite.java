package BibliotecaApp.suite;
import BibliotecaApp.control.BibliotecaCtrlTest;
import BibliotecaApp.model.CarteTest;
import BibliotecaApp.util.ValidatorTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        BibliotecaCtrlTest.class,
        CarteTest.class,
        ValidatorTest.class
})

public class TestSuite {
}
