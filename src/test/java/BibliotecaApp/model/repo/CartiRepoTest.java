package BibliotecaApp.model.repo;

import BibliotecaApp.control.BibliotecaCtrl;
import BibliotecaApp.helpers.Colors;
import BibliotecaApp.model.Carte;
import org.junit.*;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import java.util.Random;
import static junit.framework.TestCase.fail;

public class CartiRepoTest {
    private CartiRepo repo;
    private BibliotecaCtrl ctrl;
    private static String watchedLog = "";

    @Rule
    public TestWatcher watchman = new TestWatcher()
    {
        @Override
        protected void failed(Throwable e, Description description)
        {
            watchedLog+= Colors.ANSI_RED + description + " " + "HAS FAILED " + Colors.ANSI_RESET + "\n";
        }

        @Override
        protected void succeeded(Description description)
        {
            watchedLog+= Colors.ANSI_GREEN + description + " " + "HAS SUCCEEDED " + Colors.ANSI_RESET + "\n";
        }
    };

    /**
     * Generates a String containing letters a-z with a given length
     * @param length
     * @return String
     */
    public String generateString(int length)
    {
        int leftLimit = 97; // 'a'
        int rightLimit = 122; // 'z'
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(length);
        for (int i = 0; i < length; i++)
        {
            int randomLimitedInt = leftLimit + (int)(random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }

    /**
     * Prints a starting message in the console before the tests are run
     */
    @BeforeClass
    public static void beforeClass()
    {
        System.out.println(Colors.ANSI_PURPLE + "\"****** INIT UNIT TESTS ****** \n" + Colors.ANSI_RESET);
    }

    /**
     * Instantiates the repository and the controller classes
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception
    {
        repo = new CartiRepo("data/cartiDB_Test.dat");
        ctrl = new BibliotecaCtrl(repo);
    }

    /**
     * Command given after each test
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception
    {
    }

    /**
     * Testing addCarte() with all the parameters valid
     */
    @Test
    public void adaugaCarteCorrectInput()
    {
        Carte c = Carte.getCarteFromString(generateString(256)+";autorUnu,autorDoi;2018;editura1;test,autor");
        try {
            ctrl.adaugaCarte(c);
        } catch (Exception e) {
            fail();
        }
    }

    /**
     * Testing addCarte() with a null title
     * @throws Exception
     */
    @Test (expected = java.lang.Exception.class)
    public void nullTitle() throws Exception
    {
        Carte c = Carte.getCarteFromString(generateString(256)+";autorUnu,autorDoi;2018;editura1;test,autor");
        c.setTitlu(null);
            ctrl.adaugaCarte(c);
    }

    /**
     * Testing addCarte() with a title 257 characters long
     * @throws Exception
     */
    @Test (expected = java.lang.Exception.class)
    public void titleCharactersOverflow() throws Exception
    {
        Carte c = Carte.getCarteFromString(generateString(257)+";autorUnu,autorDoi;2018;editura1;test,autor");
            ctrl.adaugaCarte(c);
    }

    /**
     * Testing addCarte() with a title 255 characters long
     * @throws Exception
     */
    @Test
    public void titleCharacterNotOverflow() throws Exception
    {
        Carte c = Carte.getCarteFromString(generateString(255)+";autorUnu,autorDoi;2018;editura1;test,autor");
        ctrl.adaugaCarte(c);
    }
    /**
     * Testing addCarte() with the year being bigger than the current year
     * @throws Exception
     */
    @Test (expected = java.lang.Exception.class)
    public void inexistentYear() throws Exception
    {
        Carte c = Carte.getCarteFromString("Test;autorUnu,autorDoi;2019;editura1;test,autor");
        ctrl.adaugaCarte(c);
    }

    /**
     * Testing addCarte() having the year below 1000
     * @throws Exception
     */
    @Test (expected = java.lang.Exception.class)
    public void belowMinimumYear() throws Exception
    {
        Carte c = Carte.getCarteFromString("Test;autorUnu,autorDoi;999;editura1;test,autor");
        ctrl.adaugaCarte(c);
    }
    /**
     * Testing addCarte() with NULL authors
     * @throws Exception
     */
    @Test (expected = java.lang.Exception.class)
    public void nullAuthors() throws Exception
    {
        Carte c = Carte.getCarteFromString("Test;autorUnu;2019;editura1;test,autor");
        c.setAutori(null);
        ctrl.adaugaCarte(c);
    }

    /**
     * Testing addCarte() with the title having one character
     * @throws Exception
     */
    @Test
    public void oneCharacterTitle() throws Exception
    {
        Carte c = Carte.getCarteFromString("A;autorUnu;1990;editura1;test,autor");
        ctrl.adaugaCarte(c);
    }

    /**
     * Testing addCarte() with the title having two characters
     * @throws Exception
     */
    @Test
    public void twoCharacterTitle() throws Exception
    {
        Carte c = Carte.getCarteFromString("AA;autorUnu;1990;editura1;test,autor");
        ctrl.adaugaCarte(c);
    }

    /**
     * Prints the watchedLog from the @Rule in the console
     */
    @AfterClass
    public static void afterClass()
    {
        System.out.println(watchedLog);
        System.out.println("Finish\n\n");
    }
}