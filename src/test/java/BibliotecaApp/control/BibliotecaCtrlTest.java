package BibliotecaApp.control;

import BibliotecaApp.helpers.Colors;
import BibliotecaApp.model.Carte;
import BibliotecaApp.model.repo.CartiRepo;
import org.junit.*;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.ArrayList;
import java.util.List;


public class BibliotecaCtrlTest {

    Carte carte1 = new Carte();
    Carte carte2 = new Carte();
    CartiRepo repo = new CartiRepo("data/cartiBD.dat");
    BibliotecaCtrl ctrl = new BibliotecaCtrl(repo);
    List<Carte> carti = new ArrayList<>();
    private static String watchedLog = "";

    @Rule
    public TestWatcher watchman = new TestWatcher()
    {
        @Override
        protected void failed(Throwable e, Description description)
        {
            watchedLog+= Colors.ANSI_RED + description + " " + "HAS FAILED " + Colors.ANSI_RESET + "\n";
        }

        @Override
        protected void succeeded(Description description)
        {
            watchedLog+= Colors.ANSI_GREEN + description + " " + "HAS SUCCEEDED " + Colors.ANSI_RESET + "\n";
        }
    };

    @BeforeClass
    public static void beforeClass()
    {
        System.out.println(Colors.ANSI_PURPLE + "\"****** INIT UNIT TESTS ****** \n" + Colors.ANSI_RESET);
    }

    @Before // setup()
    public void setUp() throws Exception
    {
        carte1 = Carte.getCarteFromString("The Art of War;Sun Tzu;1550;Polirom;art,of,war,sun,tzu");
        ctrl.adaugaCarte(carte1);
    }

    @Test
    public void cautaCarteTest() throws Exception
    {
        try
        {
            Assert.assertEquals("The Art of War;Sun Tzu;1550;Polirom;art,of,war,sun,tzu",
                    ctrl.cautaCarte("Sun Tzu").get(0).toString());
        } catch (IndexOutOfBoundsException e) {
            System.out.println(e.toString());
        }
    }

    @After // tearDown()
    public void tearDown()
    {
        try
        {
            ctrl.deleteCarte(carte1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ctrl = null;
    }

    @AfterClass
    public static void afterClass()
    {
        System.out.println(watchedLog);
        System.out.println("Finish\n\n");
    }
}
