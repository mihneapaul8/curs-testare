package BibliotecaApp.repository.repoMock;

import BibliotecaApp.model.Carte;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class CartiRepoMockTest {

    private Carte c1, c2, c3;
    CartiRepoMock cartiRepoMock;

    @Before
    public void setUp() throws Exception {

        c1 = new Carte();
        c1.setTitlu("Carte1");
        c1.setAutori(Arrays.asList("X","Y"));
        c1.setAnAparitie("1885");
        c1.setEditura("Editura1");
        c1.setCuvinteCheie(Arrays.asList("Cyberpunk","Future"));

        c2 = new Carte();
        c2.setTitlu("Carte2");
        c2.setAutori(Arrays.asList("X"));
        c2.setAnAparitie("2005");
        c2.setEditura("Editura2");
        c2.setCuvinteCheie(Arrays.asList("test1","test2"));

        c3 = new Carte();
        c3.setTitlu("Carte3");
        c3.setAutori(Arrays.asList("Z"));
        c3.setAnAparitie("2018");
        c3.setEditura("Editura3");
        c3.setCuvinteCheie(Arrays.asList("test3","test4"));
        cartiRepoMock = new CartiRepoMock();


    }

    @After
    public void tearDown() throws Exception {

        c1 = c2 = c3 = null;
        cartiRepoMock = null;
    }

    @Test
    public void nullRepository() {

        assertEquals("Repository should be empty",0,cartiRepoMock.cautaCarte("Test").size());
    }
    @Test
    public void searchAuthor() {
        cartiRepoMock.adaugaCarte(c1);
        cartiRepoMock.adaugaCarte(c2);
        assertEquals("Searching for author",2,cartiRepoMock.cautaCarte("X").size());
    }

    @Test
    public void cautaCarteBibliotecaFaraAutor() {
        cartiRepoMock.adaugaCarte(c3);
        assertEquals("Searching inexistent author",0,cartiRepoMock.cautaCarte("Dorel").size());
    }
}