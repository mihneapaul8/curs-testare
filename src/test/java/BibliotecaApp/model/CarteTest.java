package BibliotecaApp.model;

import BibliotecaApp.helpers.Colors;
import org.junit.*;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class CarteTest {
    Carte carte1, carte2, carte3;
    private static String watchedLog = "";


    @Rule
    public TestWatcher watchman = new TestWatcher()
    {
        @Override
        protected void failed(Throwable e, Description description)
        {
            watchedLog+= Colors.ANSI_RED + description + " " + "HAS FAILED " + Colors.ANSI_RESET + "\n";
        }

        @Override
        protected void succeeded(Description description)
        {
            watchedLog+= Colors.ANSI_GREEN + description + " " + "HAS SUCCEEDED " + Colors.ANSI_RESET + "\n";
        }
    };

    @BeforeClass
    public static void setUpClass()
    {
        System.out.println(Colors.ANSI_PURPLE + "\"****** INIT UNIT TESTS ****** \n" + Colors.ANSI_RESET);
    }

    @Before
    public void setUp()
    {
        carte1 = Carte.getCarteFromString("The Art of War;Sun Tzu;1550;Habarnam;art,of,war,sun,tzu");
        carte2 = Carte.getCarteFromString("Blestemul Reginei;Philippa Gregory;2001;Polirom;blestemul,reginei,philippa,gregory");
        carte3 = Carte.getCarteFromString("Istoria Artei;Gombrich;1991;Polirom;istoria,artei,gombrich");
    }


    @Test
    public void getTitlu()
    {
        assertEquals("The Art of War", carte1.getTitlu());
    }

    @Test(expected = org.junit.ComparisonFailure.class)
    public void setAuthors()
    {
        String initialAuthor = carte2.getAutori().get(0);
        List<String> tempList = Collections.singletonList("Dorel");
        carte2.setAutori(tempList);
        assertEquals(initialAuthor,carte2.getAutori().get(0));
    }

    @Test
    public void getEditura()
    {
        assertEquals("Polirom",carte2.getEditura());
        assertEquals("Polirom",carte3.getEditura());
    }


    @After
    public void tearDown()
    {
        carte1 = null;
        carte2 = null;
        carte3 = null;
    }

    @AfterClass
    public static void tearDownClass()
    {
        System.out.println(watchedLog);
        System.out.println("Finish\n\n");
    }
}